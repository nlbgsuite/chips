# CHIP-YYYY-MM-Short-Descriptive-Memorable-Name

## Summary

Short and easy to understand intent of this CHIP.

- Version: (important so that statements can refer to specific versions)
- Is consensus change: (basically yes/no)
- Owner: (the individual who is accountable for organization, progress, and ultimately the outcome of this CHIP. May be one of the contributors)
- Contributors: (people who have made historical or ongoing contributions to the content of this CHIP. Being a contributor does not imply being an owner, and contributors do not necessarily share an owner's work in ensuring progress)
- Discussion URL:
- Full change history URL: (for example, a canonical git repository of your choosing)


## Motivation and Benefits

For example, impact on various stakeholder groups.


## Implementation Costs and Risks

For example, backward incompatibilities, SPV server upgrades, wallet upgrades, operational changes for businesses, risk of polarization.


## Ongoing Costs and Risks

For example, increased complexity, increased maintenance, impact on other future changes, increased operating costs, perception of holders.


## Technical Description

For example, early in the CHIP's life, basic outlines of possibilities, and later in the CHIP's life, specifically architecture, etc.


### Implementations

### Specification

### Test Cases


## Evaluation of Alternatives

For example, do nothing, plausible alternatives.


## Security considerations

For example, network DOS, application DOS, user double spends, miner double spends.


## List of major stakeholders

For example, "Wallet projects such as...", "Mining pools such as...".
Note that these are not sponsors, but stakeholders that the CHIP owner thinks need to engage with the proposal.


## Statements with version

For example, links and optionally summaries of statements from node developers, businesses, pools, miners, wallets, infrastructure operators, application developers, service operators.


## License

For example, MIT or CC0.
